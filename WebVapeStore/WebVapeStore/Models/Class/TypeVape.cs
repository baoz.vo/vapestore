﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VapeStore.Models
{
    public class TypeVape
    {
        [Key]
        public string TypeVapeId { get; set; }
        public string TypeName { get; set; }
        public bool Flag { get; set; }
    }
}