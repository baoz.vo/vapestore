﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using VapeStore.Models;
using MySql.Data.EntityFramework;

namespace WebVapeStore
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class StoreEntities : DbContext
    {
        // Your context has been configured to use a 'StoreEntities' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'VapeStore.Models.StoreEntities' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'StoreEntities' 
        // connection string in the application configuration file.
        public StoreEntities() : base()
        {
            MySqlConnection myConnection = new MySqlConnection();
            myConnection.ConnectionString = "persistsecurityinfo=True;database=vapestore;server=localhost;Connect Timeout=30;user id=root; pwd=";
            myConnection.Open();

        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Vape> Vapes { get; set; }
        public DbSet<TypeVape> TypeVapes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<BillDetail> BillDetails { get; set; }
    }
}