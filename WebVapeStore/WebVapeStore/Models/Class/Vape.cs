﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Web;

namespace VapeStore.Models
{
    public class Vape
    {
        [Key]
        public string VapeId { get; set; }
        public string VapeName { get; set; }
        public string VapeCode { get; set; }
        public string Image { get; set; }
        public double Price { get; set; }
        public string Introduce { get; set; }
        public string Describe { get; set; }
        public bool Flag { get; set; }

        public virtual TypeVape TypeVape { get; set; }
    }
}