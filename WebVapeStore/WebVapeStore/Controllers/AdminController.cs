﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebVapeStore.Models.Action;

namespace WebVapeStore.Controllers
{
    public class AdminController : Controller
    {     
        // GET: Admin
        public ActionResult Index()
        {
            //if(status == false)
            //{
            //    return RedirectToAction("Login", "Admin");
            //}
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(string Username,string Password)
        {
            string PassEncrypt = MD5Hash(Password);
            using (StoreEntities store = new StoreEntities())
            {
                var Us = store.Users.Where(c => c.Username == Username && c.Password == PassEncrypt).FirstOrDefault();
                if (Us != null)
                {
                   
                    Nortification.status = true;
                } 
                else
                {
                    Nortification.norti = Convert.ToString(Us);
                }
            }
            if (Nortification.status)
            {
                return Redirect("~/Admin/Index");
            }
            else
            {
                return Redirect("~/Admin/LogIn");
            }
        }
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

    }
}