﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VapeStore.Models
{
    public class BillDetail
    {
        [Key]
        public string BillDetailId { get; set;}
        public int Amount { get; set; }
        public double Price { get; set; }

        public virtual Bill Bill { get; set; }
        public virtual Vape Vape { get; set; }
        public virtual Coupon Coupon { get; set; }
    }
}